#!/usr/bin/env python3

import sys
import os
import shutil

arguments = sys.argv


def createTestDir(path: str):
    """Erstellt das Testverzeichnis, wenn das Testverzeichnis noch nicht existiert."""
    pathToTestDir = path + "/PTestDir"
    if not os.path.exists(pathToTestDir):
        os.mkdir(pathToTestDir)
    return pathToTestDir


def containsTestAnnotation(possibleFilePath):
    """Überprüft, ob eine Datei eine @Test Annotation beinhaltet. Wenn dies so ist,
    so wollen wir die Datei nicht kopieren, um Komplikationen mit dem PTest zu verhindern"""
    with open(possibleFilePath, 'r') as file:
        string = file.read()
        if '@Test' in string:
            return True
        else:
            return False


def containsPackageStatement(pathToFile):
    """Überprüft, ob eine Datei ein Package Statement beinhaltet"""
    with open(pathToFile, 'r') as file:
        string = file.read()
        if 'package' in string:
            return True
        else:
            return False


def readFile(path):
    """Liest eine Datei ein und gibt deren Inhalt zurueck"""
    with open(path, 'r') as file:
        string = file.readlines()
        return string


def correctFile(pathToTestDir):
    """Korrigiert eine Datei. Wenn ein package Statement enthalten ist, so wird es ersetzt"""
    for file in os.listdir(pathToTestDir):
        pathToFile = pathToTestDir + '/' + file
        if containsPackageStatement(pathToFile):
            lines = readFile(pathToFile)
            with open(pathToFile, 'w') as fileToWrite:
                outputString = ''
                for line in lines:
                    if 'package' in line:
                        print("Datei:" + pathToFile + " beinhaltet ein package Statement!")
                    else:
                        outputString += line + '\n'
                fileToWrite.write(outputString)


def copyFilesToTestDir(path, pathToTestDir):
    """Kopiert die Dateien, die in dem zum Start des Programmes angegebenen Pfades enthalten sind,
    zu einem neuen Verzeichnis namens PTestDir"""

    for files in os.listdir(path):
        possibleFilePath = path + '/' + files
        if os.path.isfile(possibleFilePath) and os.path.basename(possibleFilePath).endswith('.java'):
            if not containsTestAnnotation(possibleFilePath):
                shutil.copyfile(possibleFilePath, pathToTestDir)
    correctFile(pathToTestDir)


def start():
    """Startet die Applikation"""
    pathToTestDir = createTestDir(path)
    copyFilesToTestDir(path, pathToTestDir)


if len(arguments) != 2:
    sys.stderr.write("Usage: python3 PTestMover <path>\n")
    sys.stderr.write(f'Received {len(arguments)}\n')
    exit(1)
else:
    path = arguments[1]
    if os.path.exists(path):
        start()
    else:
        sys.stderr.write("Path doesnt exist")
        exit(1)
