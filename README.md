# PTestMover

## What it does
This script copies all .java files that do not contain the annotation @Test into a new directory named
PTestDir. If a file contains a package statement, the line will be removed. 

## How to call
1.  Change permissions via: chmod +x PTestMover.py
2.  Execute the program via ./PTestMover.py <PathToTheJavaDir>

After following these two steps, you should find a directory named PTestDir containing all relevant .java files. 
